package com.cbsystematics.homeworks;

/**
 * ArithmeticAverage Class presents homework on average operation with numbers
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ArithmeticAverage
{
    public static void main(String[] args)
    {
        System.out.println(getNumbersAverage(10, 12, 3, 4));
    }
    
    /**
     * The method calculates the result of average operation with numbers
     *
     * @param numbers any number of int numbers
     * @return float result
     */
    public static float getNumbersAverage(int... numbers) {
        int total = 0;
        
        for (int i = 0; i < numbers.length; total += numbers[i++]);
        
        return total / ((float) numbers.length);
    }
}
