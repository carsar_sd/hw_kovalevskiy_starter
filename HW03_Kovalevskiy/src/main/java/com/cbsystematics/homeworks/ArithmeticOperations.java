package com.cbsystematics.homeworks;

/**
 * ArithmeticOperations Class presents homework on arithmetic operations with numbers
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ArithmeticOperations
{
    public static void main(String[] args)
    {
        int[] result = getCalculationResult(10, 12, 3);
        
        System.out.printf("x: %d\ny: %d\nz: %d\n", result[0], result[1], result[2]);
    }
    
    /**
     * The method calculates the result of arithmetic operations
     *
     * @param x input param
     * @param y input param
     * @param z input param
     * @return int array with results
     */
    public static int[] getCalculationResult(int x, int y, int z) {
        x += y - x++ * z; // x = 10 + 12 - 30 = -8
        z = --x - y * 5;  // z = -9 - 60 = -69
        y /= x + 5 % z;   // y = 12 / (-9 + 5) = -3
        z = x++ + y * 5;  // z = -9 + -3 * 5 = -24
        x = y - x++ * z;  // x = -3 - (-8) * (-24) = -195
        
        return new int[] {x, y, z};
    }
}
