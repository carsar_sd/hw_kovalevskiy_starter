package com.cbsystematics.homeworks;

/**
 * CylinderCalculation Class presents homework on calculation of a circle params
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class CircleCalculation
{
    public static final double PI = Math.PI;
    
    public static void main(String[] args)
    {
        double result = getCircleArea(10);
        
        System.out.printf("Circle Area: %.2f\n", result);
    }
    
    /**
     * The method calculates the area of a circle
     *
     * @param r circle radius value
     * @return circle area
     */
    public static double getCircleArea(int r) {
        return PI * Math.pow(r, 2);
    }
}
