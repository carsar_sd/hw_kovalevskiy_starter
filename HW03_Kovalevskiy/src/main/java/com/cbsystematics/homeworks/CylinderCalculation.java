package com.cbsystematics.homeworks;

/**
 * CylinderCalculation Class presents homework on calculation of a cylinder params
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class CylinderCalculation
{
    public static final double PI = Math.PI;
    
    public static void main(String[] args)
    {
        double area = getCylinderArea(3, 8);
        double volume = getCylinderVolume(3, 6);
        
        System.out.printf("Cylinder Area: %.2f\n", area);
        System.out.printf("Cylinder Volume: %.2f\n", volume);
    }
    
    /**
     * The method calculates the area of a cylinder
     *
     * @param r cylinder radius value
     * @param h cylinder height value
     * @return cylinder area
     */
    public static double getCylinderArea(int r, int h) {
        return 2 * PI * r * (r + h);
    }
    
    /**
     * The method calculates the volume of a cylinder
     *
     * @param r cylinder radius value
     * @param h cylinder height value
     * @return cylinder volume
     */
    public static double getCylinderVolume(int r, int h) {
        return PI * Math.pow(r, 2) * h;
    }
}
