package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class ArithmeticAverageTest
{
    @Test
    public void testCalculationResult() {
        float result = ArithmeticAverage.getNumbersAverage(10, 12, 3, 4);
        Assert.assertEquals(7.25, result, 2);
    }
}
