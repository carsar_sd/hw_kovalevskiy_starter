package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class ArithmeticOperationsTest
{
    @Test
    public void testCalculationResult() {
        int[] result = ArithmeticOperations.getCalculationResult(10, 12, 3);
        Assert.assertArrayEquals(new int[]{-195, -3, -24}, result);
    }
}
