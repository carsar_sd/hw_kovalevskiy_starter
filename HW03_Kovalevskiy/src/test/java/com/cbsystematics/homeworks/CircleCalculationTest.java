package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class CircleCalculationTest
{
    @Test
    public void testCircleArea() {
        double result = CircleCalculation.getCircleArea(10);
        Assert.assertEquals(314.16, result, 2);
    }
}
