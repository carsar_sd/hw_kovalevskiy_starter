package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class CylinderCalculationTest
{
    @Test
    public void testCylinderArea() {
        double result = CylinderCalculation.getCylinderArea(3, 8);
        Assert.assertEquals(207.35, result, 2);
    }
    
    @Test
    public void testCylinderVolume() {
        double result = CylinderCalculation.getCylinderVolume(3, 6);
        Assert.assertEquals(169.65, result, 2);
    }
}
