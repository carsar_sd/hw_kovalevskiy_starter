package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Scanner;
import java.util.regex.Pattern;

import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

/**
 * Calculator Class presents homework on implementation of a simple calculator functionality
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Calculator {
    public static void main(String[] args) throws ScriptException {
        runConsoleCalculator();
    }
    
    /**
     * The method takes the input from the console and evaluates the result of math expression
     */
    public static void runConsoleCalculator() throws ScriptException {
        System.out.println("-> Hello! This is a simple calculator script.");
        System.out.println("-> You can operate on integers and one of four arithmetic operators to choose from.");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");
        
        Scanner in = new Scanner(System.in);
    
        int operand1 = Calculator.getConsoleIntData(in, false, (char) 0);
        char operator = Calculator.getConsoleCharData(in);
        int operand2 = Calculator.getConsoleIntData(in, true, operator);
    
        in.close();
    
        String formula = operand1 + " " + operator + " " + operand2;

        System.out.printf("-> %d %s %d = %s\n", operand1, operator, operand2, getExpressionResult(formula));
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @param extra condition
     * @param extraVal condition value
     * @return integer console data
     */
    public static int getConsoleIntData(Scanner in, boolean extra, char extraVal) {
        boolean isCorrectInt;
        int number = 0;
        
        do {
            System.out.print("-> Enter operand: ");
            isCorrectInt = in.hasNextInt();
            
            if (isCorrectInt) {
                number = in.nextInt();
    
                if (extra) {
                    if (extraVal == '/' && number == 0) {
                        isCorrectInt = false;
                        System.out.println("-> NOT OK! Division by zero is incorrect.");
                    } else {
                        System.out.println("-> OK!");
                    }
                } else {
                    System.out.println("-> OK!");
                }
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
                
                System.out.println("-> NOT OK! Your number is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectInt);
        
        return number;
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @return character console data
     */
    public static char getConsoleCharData(Scanner in) {
        boolean isCorrectChar;
        char character = 0;
        
        do {
            System.out.print("-> Input operator( + - * / ): ");
            isCorrectChar = in.hasNext(Pattern.compile("[+\\-*/]"));
    
            if (isCorrectChar) {
                character = in.next(".").charAt(0);
                System.out.println("-> OK!");
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
        
                System.out.println("-> NOT OK! Your operator is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectChar);
        
        return character;
    }
    
    /**
     * The method executes a math expression
     *
     * @param formula math expression
     * @return result of math expression
     */
    public static String getExpressionResult(String formula) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        
        return engine.eval(formula).toString();
    }
}
