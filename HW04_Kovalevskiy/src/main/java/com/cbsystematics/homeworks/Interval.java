package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.NumbersHelper;
import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Scanner;

/**
 * Interval Class presents homework on determining whether a number belongs to an interval
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Interval {
    public static void main(String[] args) {
        checkConsoleInterval();
    }
    
    /**
     * The method takes the input from the console and checks it belonging
     */
    public static void checkConsoleInterval() {
        System.out.println("-> Hello! This is a simple integer analyzer.");
        System.out.println("-> You can enter an integer and find out its belonging.");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");
    
        Scanner in = new Scanner(System.in);
    
        int number = Interval.getConsoleIntData(in);
    
        in.close();
    
        /* -- comparison block */
        String range = "";
    
        if (NumbersHelper.isBetweenInterval(number, 0, 14, false)) {
            range = "[0 - 14]";
        } else if (NumbersHelper.isBetweenInterval(number, 15, 35, false)) {
            range = "[15 - 35]";
        } else if (NumbersHelper.isBetweenInterval(number, 36, 50, false)) {
            range = "[36 - 50]";
        } else if (NumbersHelper.isBetweenInterval(number, 51, 100, false)) {
            range = "[50 - 100]";
        }
        /* // comparison block */
    
        System.out.printf("-> The entered number belongs to %s range!\n", range);
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @return integer console data
     */
    public static int getConsoleIntData(Scanner in) {
        boolean isCorrectInt;
        int number = 0;
        
        do {
            System.out.print("-> Input a number between 0 and 100: ");
            isCorrectInt = in.hasNextInt();
            
            if (isCorrectInt) {
                number = in.nextInt();
    
                if (NumbersHelper.isBetweenInterval(number, 0, 100, false)) {
                    System.out.println("-> OK!");
                } else {
                    isCorrectInt = false;
                    System.out.println("-> NOT OK! Number is out of range [0-100].");
                }
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
                
                System.out.println("-> NOT OK! Your number is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectInt);
        
        return number;
    }
}
