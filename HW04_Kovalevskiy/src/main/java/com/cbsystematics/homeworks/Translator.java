package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Translator Class presents homework on text translation by switch / google
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Translator {
    public static void main(String[] args) {
        runConsoleTranslator();
    }
    
    /**
     * The method takes the input from the console and translate it
     */
    public static void runConsoleTranslator()
    {
        System.out.println("-> Hello! This is a simple translator.");
        System.out.println("-> You can enter a text in Russian and the script will translate it in English.");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");
        
        Scanner in = new Scanner(System.in);
        
        String text = Translator.getConsoleStringData(in);

        in.close();

        System.out.printf("-> %s -> %s\n", text, switchTranslate(text));
        
        // works, but has small free quota
        //System.out.printf("-> %s -> %s\n", text, googleTranslate("ru", "en", text));
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @return string console data
     */
    public static String getConsoleStringData(Scanner in) {
        boolean isCorrectString;
        String text = "";
    
        do {
            System.out.print("-> Input a text in Russian: ");
            isCorrectString = in.hasNext(
                    Pattern.compile("[\\p{InCyrillic}\\p{Space}]*", Pattern.UNICODE_CHARACTER_CLASS)
            );
        
            if (isCorrectString) {
                text = in.nextLine();
                System.out.println("-> OK! ");
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
            
                System.out.println("-> NOT OK! Your text is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectString);
        
        return text;
    }
    
    /**
     * The method performs text translation using switch operator
     *
     * @param text text to translate
     * @return translated text
     */
    public static String switchTranslate(String text) {
        String result = "none";
        
        if (text.length() != 0) {
            switch (text) {
                case "ветер":
                    result = "wind";
                    break;
                case "дождь":
                    result = "rain";
                    break;
                case "снег":
                    result = "snow";
                    break;
                case "солнце":
                    result = "sun";
                    break;
                case "ветер западный":
                    result = "western wind";
                    break;
                case "скорость ветра":
                    result = "wind speed";
                    break;
                default:
                    result = "entered text is unknown";
            }
        }
        
        return result;
    }
    
    /**
     * The method performs text translation using google translator
     *
     * @param langFrom translate from lang
     * @param langTo translate to lang
     * @param text text to translate
     * @return translated text
     */
    public static String googleTranslate(String langFrom, String langTo, String text) throws IOException {
        String urlStr = "https://script.google.com/macros/s/AKfycbynotI6gzWYO1nwoPP3XSBjockrgPsPCN2f2YlvdABEzUcnP2Or/exec" +
                "?q=" + URLEncoder.encode(text, "UTF-8") +
                "&target=" + langTo +
                "&source=" + langFrom;
        URL url = new URL(urlStr);

        StringBuilder response = new StringBuilder();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("User-Agent", "Mozilla/5.0");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return response.toString();
    }
}
