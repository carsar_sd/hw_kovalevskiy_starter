package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

import javax.script.ScriptException;

public class CalculatorTest {
    @Test
    public void testGetExpressionResult() throws ScriptException {
        String resultTrue = Calculator.getExpressionResult("4 * 2 / (2 + 2)");
        Assert.assertEquals("2", resultTrue);
    }
}

/*
 - какое кол-во проверок стоит делать на простой функционал?
 - нужно ли проверять на Equals и NotEquals или достаточно только одной на Equals?
*/