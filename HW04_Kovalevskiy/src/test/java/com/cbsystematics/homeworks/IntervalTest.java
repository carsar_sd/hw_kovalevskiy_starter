package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.NumbersHelper;
import org.junit.Assert;
import org.junit.Test;

public class IntervalTest {
    @Test(expected = IllegalArgumentException.class)
    public void testIsBetween() {
        boolean resultTrue = NumbersHelper.isBetweenInterval(10, 8, 15, false);
        Assert.assertTrue(resultTrue);
        
        boolean resultFalse = NumbersHelper.isBetweenInterval(5, 8, 15, false);
        Assert.assertFalse(resultFalse);
        
        boolean resultException = NumbersHelper.isBetweenInterval(10, 18, 15, false);
    }
}
