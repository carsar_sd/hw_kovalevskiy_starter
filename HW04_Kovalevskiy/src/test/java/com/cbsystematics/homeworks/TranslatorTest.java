package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class TranslatorTest {
    @Test
    public void testSwitchTranslate() {
        String resultTrue = Translator.switchTranslate("ветер");
        Assert.assertEquals("wind", resultTrue);
    
        String resultFalse = Translator.switchTranslate("любой текст");
        Assert.assertEquals("entered text is unknown", resultFalse);
    
        String resultNone = Translator.switchTranslate("");
        Assert.assertEquals("none", resultNone);
    }
    
    @Test
    public void testGoogleTranslate() throws IOException {
        String resultGoogle = Translator.googleTranslate("ru", "en", "консольный переводчик");
        Assert.assertEquals("console translator", resultGoogle);
    }
}
