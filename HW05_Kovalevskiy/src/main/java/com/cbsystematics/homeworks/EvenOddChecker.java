package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * LogicOperations Class presents homework on parity determination
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class EvenOddChecker {
    public static void main(String[] args) {
        runEvenOddChecker();
    }
    
    /**
     * The method takes the input from the console and runs checking process
     */
    public static void runEvenOddChecker() {
        System.out.println("-> Hello! This is a simple number odd even checker.");
        System.out.println("-> You can use an integer to find out if it is even or odd.");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");

        Scanner in = new Scanner(System.in);
    
        int number = EvenOddChecker.getConsoleIntData(in, true);
        
        in.close();
        
        /* Все 3 метода рабочие! */
        String result = checkEvenOddDiv(number);
        //String result = checkEvenOddBinary(number);
        //String result = checkEvenOddLogic(number);
        
        System.out.println(result);
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @param extra condition
     * @return integer console data
     */
    public static int getConsoleIntData(Scanner in, boolean extra) {
        boolean isCorrectInt;
        int number = 0;
        
        do {
            System.out.print("-> Input operand: ");
            isCorrectInt = in.hasNextInt();
            
            if (isCorrectInt) {
                number = in.nextInt();
                
                if (extra) {
                    if (number != 0) {
                        System.out.println("-> OK!");
                    } else {
                        isCorrectInt = false;
                        System.out.println("-> NOT OK! Number cannot be zero!");
                    }
                } else {
                    System.out.println("-> OK!");
                }
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
                
                System.out.println("-> NOT OK! Your operand is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectInt);
        
        return number;
    }
    
    /**
     * The method checks the number using division
     *
     * @param number input ineger checked param
     * @return result of checking
     */
    public static String checkEvenOddDiv(int number) {
        return (number % 2 == 0)
                ? "-> Число " + number + " является четным!"
                : "-> Число " + number + " является не четным!";
    }
    
    /**
     * The method checks the number using binary
     *
     * @param number input ineger checked param
     * @return result of checking
     */
    public static String checkEvenOddBinary(int number) {
        Pattern pattern = Pattern.compile("0$");
        Matcher matcher = pattern.matcher(Integer.toBinaryString(number));
    
        return (matcher.find())
                ? "-> Число " + number + " является четным!"
                : "-> Число " + number + " является не четным!";
    }
    
    /**
     * The method checks the number using logic
     *
     * @param number input ineger checked param
     * @return result of checking
     */
    public static String checkEvenOddLogic(int number) {
        return ((number & 1) == 0)
                ? "-> Число " + number + " является четным!"
                : "-> Число " + number + " является не четным!";
    }
}
