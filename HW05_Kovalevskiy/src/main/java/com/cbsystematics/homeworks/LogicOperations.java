package com.cbsystematics.homeworks;

/**
 * LogicOperations Class presents homework on using arithmetic and logical operations
 * Main point is operations priority
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class LogicOperations {
    public static void main(String[] args) {
        int x = 5, y = 10, z = 15;
    
        x += y >> x++ * z; // 6 + (10 >> 5 * 15) -> 5 + 10/2^75 = 5
        z = ++x & y * 5;   // 6 & 50 = 2
        y /= x + 5 | z;    // 10 / (6 + 5 | 2) -> 10 / (11 | 2) = 0
        z = x++ & y * 5;   // 6 & 0 = 0
        x = y << x++ ^ z;  // 0 << 7 ^ 0 -> 0 ^ 0 = 0
    
        System.out.println("x: " + x + " | y: " + y + " | z: " + z);
    }
}
