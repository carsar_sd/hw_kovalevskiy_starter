package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * PowChecker Class presents homework on checking if a number equals pow(2, y)
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class PowChecker {
    public static void main(String[] args) {
        runPowChecker();
    }
    
    /**
     * The method takes the input from the console and runs checking process
     */
    public static void runPowChecker() {
        System.out.println("-> Hello! This is a simple number pow checker.");
        System.out.println("-> You can use an integer to find out if the number equals pow(2, y).");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");
        
        Scanner in = new Scanner(System.in);
        
        int number = PowChecker.getConsoleIntData(in, true);
        
        in.close();
        
        /* Все 3 метода рабочие! */
        String result = checkPowNumberLoop(number).replace("[N]", Integer.toString(number));
        //String result = checkPowNumberRecursion(number).replace("[N]", Integer.toString(number));
        //String result = checkPowNumberBinary(number).replace("[N]", Integer.toString(number));
        
        System.out.println(result);
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @param extra condition
     * @return integer console data
     */
    public static int getConsoleIntData(Scanner in, boolean extra) {
        boolean isCorrectInt;
        int number = 0;
        
        do {
            System.out.print("-> Input operand: ");
            isCorrectInt = in.hasNextInt();
            
            if (isCorrectInt) {
                number = in.nextInt();
                
                if (extra) {
                    if (number > 0) {
                        System.out.println("-> OK!");
                    } else {
                        isCorrectInt = false;
                        System.out.println("-> NOT OK! Number must be more then 0(zero)!");
                    }
                } else {
                    System.out.println("-> OK!");
                }
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
                
                System.out.println("-> NOT OK! Your operand is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectInt);
        
        return number;
    }
    
    /**
     * The method checks the number using loop
     *
     * @param number input ineger checked param
     * @return result of checking
     */
    public static String checkPowNumberLoop(int number) {
        while(number != 1) {
            if (number % 2 == 0) {
                number /= 2;
            } else {
                return "-> Число [N] не является степенью числа 2!";
            }
        }
    
        return "-> Число [N] является степенью числа 2!";
    }
    
    /**
     * The method checks the number using recursion
     *
     * @param number input ineger checked param
     * @return result of checking
     */
    public static String checkPowNumberRecursion(int number) {
        if (number != 1) {
            if (number % 2 == 0) {
                return checkPowNumberRecursion(number / 2);
            } else {
                return "-> Число [N] не является степенью числа 2!";
            }
        }
    
        return "-> Число [N] является степенью числа 2!";
    }
    
    /**
     * The method checks the number using binary value
     *
     * @param number input ineger checked param
     * @return result of checking
     */
    public static String checkPowNumberBinary(int number) {
        Pattern pattern = Pattern.compile("^1(0)*$");
        Matcher matcher = pattern.matcher(Integer.toBinaryString(number));
        
        return (matcher.find())
                ? "-> Число [N] является степенью числа 2!"
                : "-> Число [N] не является степенью числа 2!";
    }
}
