package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.objects.Employee;
import com.cbsystematics.homeworks.services.EmployeeService;

import java.io.IOException;
import java.util.List;

/**
 * Premium Class presents homework on calculating employee bonus value
 * Employee list retrieved from auto-generated JSON file
 * Used GSon and Jackson libs
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Premium {
    public static final String JSON_PATH = "employees.json";
    
    public static void main(String[] args) throws IOException {
        List<Employee> employeesGSon = EmployeeService.getEmployeesGSon(Premium.JSON_PATH);
        List<Employee> employeesJackson = EmployeeService.getEmployeesJackson(Premium.JSON_PATH);
    
        /* оба списка employeesGSon / employeesJackson рабочие */
        employeesJackson.forEach((employee) -> {
            int experience = employee.getExperience();
            int salary = employee.getSalary();
            float bonus = employee.getBonus(experience, salary);
    
            System.out.printf("%s %s, experience: %d, bonus: %.2f\n",
                    employee.getFirstName(),
                    employee.getLastName(),
                    experience,
                    bonus
            );
        });
    }
}
