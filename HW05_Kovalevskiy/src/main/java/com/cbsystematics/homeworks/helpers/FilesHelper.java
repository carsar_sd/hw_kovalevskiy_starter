package com.cbsystematics.homeworks.helpers;

import com.google.common.io.Resources;

import java.io.File;

/**
 * FilesHelper Class is a helper class for different work with files
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public final class FilesHelper {
    /**
     * The method determines if a file exists
     *
     * @param path file path to check
     * @return existence test result
     */
    public static boolean isFileExists(String path) {
        return new File(path).exists();
    }
    
    /**
     * The method determines if a resource exists
     *
     * @param path resource path to check
     * @return existence test result
     */
    public static boolean isResourceExists(String path) {
        return Resources.getResource(path) != null;
    }
}
