package com.cbsystematics.homeworks.objects;

import com.cbsystematics.homeworks.helpers.NumbersHelper;

/**
 * Data object class
 */
public class Employee {
    
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private int experience;
    private int salary;
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getGender() {
        return this.gender;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public int getExperience() {
        return this.experience;
    }
    
    public void setExperience(int experience) {
        this.experience = experience;
    }
    
    public int getSalary() {
        return this.salary;
    }
    
    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + this.id +
                ", firstName='" + this.firstName + '\'' +
                ", lastName='" + this.lastName + '\'' +
                ", email='" + this.email + '\'' +
                ", gender='" + this.gender + '\'' +
                ", experience=" + this.experience +
                ", salary=" + this.salary +
                '}';
    }
    
    /**
     * Method calculates employee bonus value
     *
     * @param experience
     * @param salary
     * @return
     */
    public float getBonus(int experience, int salary) {
        float bonus = 0;
        
        if (NumbersHelper.isLessThanNumber(experience, 5, true)) {
            bonus = (float) (salary * 0.1);
        } else if (NumbersHelper.isBetweenInterval(experience, 5, 9, false)) {
            bonus = (float) (salary * 0.15);
        } else if (NumbersHelper.isBetweenInterval(experience, 10, 14, false)) {
            bonus = (float) (salary * 0.25);
        } else if (NumbersHelper.isBetweenInterval(experience, 15, 19, false)) {
            bonus = (float) (salary * 0.35);
        } else if (NumbersHelper.isBetweenInterval(experience, 20, 24, false)) {
            bonus = (float) (salary * 0.45);
        } else if (NumbersHelper.isMoreThanNumber(experience, 25, false)) {
            bonus = (float) (salary * 0.5);
        }
        
        return bonus;
    }
    
    /* using Builder constructor
    
    public static class Builder {
        private final int id;
        private String firstName = "";
        private String lastName = "";
        private String email = "";
        private String gender = "";
        private int experience = 0;
        private int salary = 0;;
    
        public Builder(int id) {
            this.id = id;
        }
    
        public Builder setFirstName(String val) {
            this.firstName = val;
            return this;
        }
    
        public Builder setLastName(String val) {
            this.lastName = val;
            return this;
        }
        
        public Builder setEmail(String val) {
            this.email = val;
            return this;
        }
    
        public Builder setGender(String val) {
            this.gender = val;
            return this;
        }
    
        public Builder setExperience(int val) {
            this.experience = val;
            return this;
        }
    
        public Builder setSalary(int val) {
            this.salary = val;
            return this;
        }
    }
    
    public Employee(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.gender = builder.gender;
        this.experience = builder.experience;
        this.salary = builder.salary;
    }*/
}
