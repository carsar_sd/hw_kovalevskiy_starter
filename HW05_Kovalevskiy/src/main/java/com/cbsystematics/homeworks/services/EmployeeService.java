package com.cbsystematics.homeworks.services;

import com.cbsystematics.homeworks.helpers.FilesHelper;
import com.cbsystematics.homeworks.objects.Employee;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Data service class
 */
public class EmployeeService {
    
    /**
     * Getting a list of data using the GSon library
     * employees.json - auto-generated JSON file(mockaroo)
     *
     * @param jsonPath resource path to json file
     *
     * @return Employees List
     */
    public static List<Employee> getEmployeesGSon(String jsonPath) throws IOException {
        if (FilesHelper.isResourceExists(jsonPath)) {
            InputStream inputStream = Resources.getResource(jsonPath).openStream();
            String json = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    
            return new Gson().fromJson(json, new TypeToken<List<Employee>>() {}.getType());
        } else {
            throw new FileNotFoundException("The json file at the specified path is missing");
        }
    }
    
    /**
     * Getting a list of data using the Jackson library
     * employees.json - auto-generated JSON file(mockaroo)
     *
     * @param jsonPath resource path to json file
     *
     * @return Employees List
     */
    public static List<Employee> getEmployeesJackson(String jsonPath) throws IOException {
        if (FilesHelper.isResourceExists(jsonPath)) {
            InputStream inputStream = Resources.getResource(jsonPath).openStream();
    
            return new ObjectMapper().readValue(inputStream, new TypeReference<List<Employee>>() {});
        } else {
            throw new FileNotFoundException("The json file at the specified path is missing");
        }
    }
}
