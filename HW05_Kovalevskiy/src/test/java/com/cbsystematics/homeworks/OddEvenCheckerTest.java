package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class OddEvenCheckerTest {
    @Test
    public void testCheckvenOddDiv() {
        int[] numbersTrue  = {2, 4, 8, 122, 256, 1512};
        int[] numbersFalse = {1, 3, 9, 121, 255, 1511};
        
        for (int number: numbersTrue) {
            String resultTrue = EvenOddChecker.checkEvenOddDiv(number);
            Assert.assertEquals("-> Число " + number + " является четным!", resultTrue);
        }
    
        for (int number: numbersFalse) {
            String resultFalse = EvenOddChecker.checkEvenOddDiv(number);
            Assert.assertEquals("-> Число " + number + " является не четным!", resultFalse);
        }
    }
    
    @Test
    public void testCheckvenOddBinary() {
        int[] numbersTrue  = {2, 4, 8, 122, 256, 1512};
        int[] numbersFalse = {1, 3, 9, 121, 255, 1511};
        
        for (int number: numbersTrue) {
            String resultTrue = EvenOddChecker.checkEvenOddBinary(number);
            Assert.assertEquals("-> Число " + number + " является четным!", resultTrue);
        }
        
        for (int number: numbersFalse) {
            String resultFalse = EvenOddChecker.checkEvenOddBinary(number);
            Assert.assertEquals("-> Число " + number + " является не четным!", resultFalse);
        }
    }
    
    @Test
    public void testCheckvenOddLogic() {
        int[] numbersTrue  = {2, 4, 8, 122, 256, 1512};
        int[] numbersFalse = {1, 3, 9, 121, 255, 1511};
        
        for (int number: numbersTrue) {
            String resultTrue = EvenOddChecker.checkEvenOddLogic(number);
            Assert.assertEquals("-> Число " + number + " является четным!", resultTrue);
        }
        
        for (int number: numbersFalse) {
            String resultFalse = EvenOddChecker.checkEvenOddLogic(number);
            Assert.assertEquals("-> Число " + number + " является не четным!", resultFalse);
        }
    }
}
