package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class PowCheckerTest {
    @Test
    public void testCheckPowNumberLoop() {
        int[] numbers = {1, 2, 4, 8, 128, 256, 512};
    
        for (int number: numbers) {
            String resultTrue = PowChecker.checkPowNumberLoop(number);
            Assert.assertEquals("-> Число [N] является степенью числа 2!", resultTrue);
        }
    
        String resultFalse = PowChecker.checkPowNumberLoop(133);
        Assert.assertEquals("-> Число [N] не является степенью числа 2!", resultFalse);
    }
    
    @Test
    public void testCheckPowNumberRecursion() {
        int[] numbers = {1, 2, 4, 8, 128, 256, 512};
    
        for (int number: numbers) {
            String resultTrue = PowChecker.checkPowNumberRecursion(number);
            Assert.assertEquals("-> Число [N] является степенью числа 2!", resultTrue);
        }
    
        String resultFalse = PowChecker.checkPowNumberRecursion(133);
        Assert.assertEquals("-> Число [N] не является степенью числа 2!", resultFalse);
    }
    
    @Test
    public void testCheckPowNumberBinary() {
        int[] numbers = {1, 2, 4, 8, 128, 256, 512};
    
        for (int number: numbers) {
            String resultTrue = PowChecker.checkPowNumberBinary(number);
            Assert.assertEquals("-> Число [N] является степенью числа 2!", resultTrue);
        }
    
        String resultFalse = PowChecker.checkPowNumberBinary(133);
        Assert.assertEquals("-> Число [N] не является степенью числа 2!", resultFalse);
    }
}
