package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.NumbersHelper;

/**
 * Delivery Class presents homework for calculation factorial using loops
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Delivery {
    public static void main(String[] args) {
        int clients = 10;
    
        long resultLoops = Delivery.getDeliveryOptions(clients);
        long resultRecursion = NumbersHelper.factorial(clients);
    
        System.out.printf("resultLoops: %d\nresRecursion: %d\n", resultLoops, resultRecursion);
    }
    
    /**
     * The method takes clients count and calculate routes count using loops
     *
     * @param clients count
     * @return result of calculation
     */
    public static long getDeliveryOptions(int clients) {
        if (clients == 1) {
            return 1;
        }
        
        long count = 1;
        
        do {
            count *= clients--;
        } while (clients > 1);
        
        return count;
    }
}
