package com.cbsystematics.homeworks;

/**
 * Rectangle Class presents homework on "drawing" figures using loops
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Rectangle {
    public static void main(String[] args) {
        int height = 6, weight = 14;
    
        Rectangle.drawRectangle(height, weight);
    }
    
    /**
     * The method takes size params and draw rectangle
     *
     * @param height of the rectangle
     * @param width of the rectangle
     */
    public static void drawRectangle(int height, int width) {
        String sign;
        
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                sign = (i == 1 || i == height) ? "*" : " ";
                sign = (j == 1 || j == width)  ? "*" : sign;
                
                System.out.print(sign + (j == width ? "\n" : ""));
            }
        }
    }
}
