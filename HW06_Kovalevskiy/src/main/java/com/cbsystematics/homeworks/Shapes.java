package com.cbsystematics.homeworks;

/**
 * Rectangle Class presents homework on "drawing" figures using loops
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Shapes {
    public static void main(String[] args) {
        int height1 = 10, height2 = 19, weight1 = 10, weight2 = 19;
    
        System.out.println("1. Right Triangle");
        Shapes.drawRightTriangle(height1, weight1);
        System.out.println("\n2. Equilateral Triangle");
        Shapes.drawEquilateralTriangle(height1, weight2);
        System.out.println("\n2. Rhombus");
        Shapes.drawRhombus(height2, weight2);
    }
    
    /**
     * The method takes size params and draw a right triangle
     *
     * @param height of the triangle
     * @param width of the triangle
     */
    public static void drawRightTriangle(int height, int width) {
        String sign;
        
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                sign = (i == height) ? "*" : " ";
                sign = (j == 1 || j == i) ? "*" : sign;
                
                System.out.print(sign + (j == width ? "\n" : ""));
            }
        }
    }
    
    /**
     * The method takes size params and draw a equilateral triangle
     *
     * @param height of the triangle
     * @param width of the triangle
     */
    public static void drawEquilateralTriangle(int height, int width) {
        String sign;
        int middle = width/2 + 1;
        
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                sign = (i == height) ? "*" : " ";
                sign = ((i == 1 && j == middle) || (i != 1 && (j == middle - i + 1 || j == middle + i - 1))) ? "*" : sign;
            
                System.out.print(sign + (j == width ? "\n" : ""));
            }
        }
    }

    /**
     * The method takes size params and draw a rhombus
     *
     * @param height of the rhombus
     * @param width of the rhombus
     */
    public static void drawRhombus(int height, int width) {
        String sign;
        int middleH = height/2 + 1;
        int middleW = width/2 + 1;

        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                if (i < middleH) {
                    sign = ((i == 1 && j == middleW) || (i != 1 && (j == middleW - i + 1 || j == middleW + i - 1))) ? "*" : " ";
                } else if (i == middleH) {
                    sign = (j == 1 || j == width) ? "*" : " ";
                } else {
                    sign = ((i == height && j == middleW) || (i != height && (j == i - middleW + 1 || j == width - (i - middleW)))) ? "*" : " ";
                }
            
                System.out.print(sign + (j == width ? "\n" : ""));
            }
        }
    }
}
