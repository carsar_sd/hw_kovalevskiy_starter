package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.NumbersHelper;
import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Scanner;

/**
 * SumMin Class presents homework on calculating arithmetical operations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class SumMin {
    public static void main(String[] args) {
        runConsoleLoops();
    }
    
    /**
     * The method takes the input from the console and evaluates the result of math expression
     */
    public static void runConsoleLoops() {
        System.out.println("-> Hello! This is a simple numbers manipulator.");
        System.out.println("-> You can operate on integers.");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");
        
        Scanner in = new Scanner(System.in);
        
        int operand1 = SumMin.getConsoleIntData(in, false, 0);
        int operand2 = SumMin.getConsoleIntData(in, true, operand1);
        
        in.close();
    
        System.out.println("--");
    
        int task1Result = SumMin.runTask1(operand1, operand2);
        String task2Result = SumMin.runTask2(operand1, operand2);
    
        System.out.printf("-> Sum of the numbers between %d and %d: %d\n",
                operand1,
                operand2,
                task1Result);
        System.out.printf("-> List of even numbers between %d and %d: %s\n",
                operand1,
                operand2,
                task2Result);
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @param extra condition
     * @param extraVal condition value
     * @return integer console data
     */
    public static int getConsoleIntData(Scanner in, boolean extra, int extraVal) {
        boolean isCorrectInt;
        int number = 0;
        
        do {
            System.out.print("-> Input operand: ");
            isCorrectInt = in.hasNextInt();
    
            if (isCorrectInt) {
                number = in.nextInt();
                
                if (extra) {
                    if (NumbersHelper.isMoreThanNumber(number, extraVal, true)) {
                        System.out.println("-> OK!");
                    } else {
                        isCorrectInt = false;
                        System.out.println("-> NOT OK! operand2 must be more than operand1");
                    }
                } else {
                    System.out.println("-> OK!");
                }
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
        
                System.out.println("-> NOT OK! Your operand is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectInt);
        
        return number;
    }
    
    /**
     * The method calculates sum of the numbers between operand1 and operand2
     *
     * @param operand1
     * @param operand2
     * @return result of calculates
     */
    public static int runTask1(int operand1, int operand2) {
        int sum = 0;
        
        for (int i = operand1; i < operand2; i++) {
            if (i > operand1) {
                sum += i;
            }
        }
    
        return sum;
    }
    
    /**
     * The method prints all even numbers between operand1 and operand2
     *
     * @param operand1
     * @param operand2
     * @return result of calculates
     */
    public static String runTask2(int operand1, int operand2) {
        StringBuilder even = new StringBuilder();
        
        for (int i = operand1; i < operand2; ++i) {
            if (i > operand1 && i % 2 == 0) {
                even.append(i).append(", ");
            }
        }
    
        return even.toString().replaceAll(", $", "");
    }
}
