package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class DeliveryTest {
    @Test
    public void testGetDeliveryOptions() {
        long resultTrue = Delivery.getDeliveryOptions(10);
        Assert.assertEquals(3628800, resultTrue);
    
        long resultFalse = Delivery.getDeliveryOptions(10);
        Assert.assertNotEquals(3628801, resultFalse);
    }
}
