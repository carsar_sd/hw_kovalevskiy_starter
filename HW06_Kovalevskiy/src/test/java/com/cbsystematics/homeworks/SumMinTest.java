package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class SumMinTest {
    @Test
    public void testRunTask1() {
        int resultTrue = SumMin.runTask1(10, 15);
        Assert.assertEquals(50, resultTrue);

        int resultFalse = SumMin.runTask1(10, 12);
        Assert.assertNotEquals(12, resultFalse);
    }
    
    @Test
    public void testRunTask2() {
        String resultTrue = SumMin.runTask2(10, 16);
        Assert.assertEquals("12, 14", resultTrue);
        
        String resultFalse = SumMin.runTask2(10, 13);
        Assert.assertNotEquals("12,", resultFalse);
    }
}
