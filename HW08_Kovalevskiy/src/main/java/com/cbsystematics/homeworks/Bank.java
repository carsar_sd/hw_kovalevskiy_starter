package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;
import com.cbsystematics.homeworks.objects.Client;

import java.util.Scanner;

public class Bank {
    public static void main(String[] args) {
        Client client = new Client.Builder(1, "Yevhen", "Kovalevskyi")
                .setEmail("carsar.sd@gmail.com")
                .setGender("male")
                .setBalance(-1200.0f)
                .setCurrency("UAH")
                .build();
    
        Bank.runClientWorker(client);
    }
    
    public static void runClientWorker(Client client) {
        int amount;
        
        System.out.println("" +
                "-> Hello! This is a simple loan repayment program.\n" +
                "-> You can use an integer to update a client balance.\n" +
                "-> If you want to exit this program - type \"exit\" !\n"
        );
    
        Bank.printClientData(client);
    
        Scanner in = new Scanner(System.in);
    
        /* -- check balance */
        float currentAmount = client.getBalance();
        boolean isDebt = currentAmount < 0;
        /* // check balance */
    
        while (isDebt) {
            amount = Bank.getConsoleIntData(in, true);
            
            if (amount == 0) {
                break;
            }
    
            /* -- update balance */
            currentAmount += amount;
            client.setBalance(currentAmount);
            isDebt = currentAmount < 0;
            /* // update balance */
            
            if (isDebt) {
                Bank.printClientData(client);
            }
        }
        
        in.close();
    
        Bank.printClientData(client);
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @param extra condition
     * @return Client instance
     */
    public static int getConsoleIntData(Scanner in, boolean extra) {
        boolean isCorrectAmount;
        int amount = 0;
    
        do {
            System.out.print("-> Enter the amount: ");
            isCorrectAmount = in.hasNextInt();
            
            if (isCorrectAmount) {
                amount = in.nextInt();
    
                if (extra) {
                    if (amount != 0) {
                        System.out.println("-> OK!");
                    } else {
                        isCorrectAmount = false;
                        System.out.println("-> NOT OK! Amount can't be 0(zero)!");
                    }
                } else {
                    System.out.println("-> OK!");
                }
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitScanner(in);
                    return 0;
                }
                
                System.out.println("-> NOT OK! Your amount is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectAmount);
        
        return amount;
    }
    
    /**
     * Print object data
     *
     * @param client Client instance
     */
    public static void printClientData(Client client) {
        System.out.printf("%s %s, balance: %.2f %s\n",
                client.getFirstName(),
                client.getLastName(),
                client.getBalance(),
                client.getCurrency());
    }
}
