package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Calculator Class presents homework on simple arithmetic calculation
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Calculator {
    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(Calculator.runCalculate())
        );
    }
    
    /**
     * The method takes input from the console and returns the calculation result
     *
     * @return the calculation result
     */
    public static float[] runCalculate() {
        System.out.println("" +
                "-> Hello! This is a simple number calculation method.\n" +
                "-> You can use an integer to find out the division result.\n" +
                "-> If you want to exit this program - type \"exit\" !\n"
        );
    
        Scanner in = new Scanner(System.in);
    
        int number1 = Calculator.getConsoleIntData(in);
        int number2 = Calculator.getConsoleIntData(in);
        int number3 = Calculator.getConsoleIntData(in);
    
        in.close();
        
        return Calculator.calculate(number1, number2, number3);
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @return int number
     */
    public static int getConsoleIntData(Scanner in) {
        boolean isCorrectNumber;
        int number = 0;
        
        do {
            System.out.print("-> Input a number: ");
            isCorrectNumber = in.hasNextInt();
        
            if (isCorrectNumber) {
                number = in.nextInt();
                System.out.println("-> OK!");
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
            
                System.out.println("-> NOT OK! Your operand is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectNumber);
        
        return number;
    }
    
    /**
     * The method takes integers and returns float result of the calculation
     *
     * @param x
     * @param y
     * @param z
     * @return the calculation result
     */
    public static float[] calculate(int x, int y, int z) {
        return new float[] {(float) x/5, (float) y/5, (float) z/5};
    }
}

/*
 - Очень неопределенное задание!
 - Удалось вынести взяимодействие с консолью в отдельный метод - выглядит намного приятней!
*/