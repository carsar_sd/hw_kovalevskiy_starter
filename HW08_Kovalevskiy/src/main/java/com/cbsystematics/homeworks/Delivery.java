package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.NumbersHelper;

/**
 * Delivery Class presents homework on calculating factorial using recursion
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Delivery {
    public static void main(String[] args) {
        int number = 20;
    
        System.out.println("Factorial of the number " + number + " is: " + NumbersHelper.factorial(number));
    }
}

/*
 - не желательно использовать по причине возможности получить StackOverflowException на больших числах ???
 - при шаге рекурсии/цикла в 1 - цикл будет быстрее по скорости ???
 - https://www.quora.com/Why-do-people-say-recursive-function-is-a-bad-practice-in-programming
*/