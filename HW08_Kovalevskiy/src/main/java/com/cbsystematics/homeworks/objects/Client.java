package com.cbsystematics.homeworks.objects;

public class Client {
    
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private float balance;
    private String currency;
    
    public Client() {
    
    }
    
    public Client(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.gender = builder.gender;
        this.balance = builder.balance;
        this.currency = builder.currency;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getGender() {
        return gender;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public float getBalance() {
        return balance;
    }
    
    public void setBalance(float balance) {
        this.balance = balance;
    }
    
    public String getCurrency() {
        return currency;
    }
    
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", balance=" + balance + '\'' +
                ", currency=" + currency +
                '}';
    }
    
    public static class Builder {
        private final int id;
        private final String firstName;
        private final String lastName;
        private String email = "";
        private String gender = "";
        private float balance = 0.0f;
        private String currency = "";
        
        public Builder(int id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }
        
        public Builder setEmail(String val) {
            this.email = val;
            return this;
        }
        
        public Builder setGender(String val) {
            this.gender = val;
            return this;
        }
        
        public Builder setBalance(float val) {
            this.balance = val;
            return this;
        }
    
        public Builder setCurrency(String val) {
            this.currency = val;
            return this;
        }
    
        public Client build() {
            return new Client(this);
        }
    }
}
