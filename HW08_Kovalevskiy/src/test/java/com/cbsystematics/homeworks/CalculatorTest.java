package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CalculatorTest {
    @Test
    public void testCalculate() {
        int[] numbersTrue = {10, 15, 22};
        
        float[] resultTrue = Calculator.calculate(numbersTrue[0], numbersTrue[1], numbersTrue[2]);
        Assert.assertTrue(Arrays.equals(new float[] {2.0f, 3.0f, 4.4f}, resultTrue));
    
        int[] numbersFalse = {128, 256, 512};
        float[] resultFalse = Calculator.calculate(numbersFalse[0], numbersFalse[1], numbersFalse[2]);
        Assert.assertFalse(Arrays.equals(new float[] {2.0f, 3.0f, 4.4f}, resultFalse));
    }
}
