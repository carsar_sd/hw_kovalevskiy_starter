package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.NumbersHelper;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class DeliveryTest {
    @Test
    public void testCalculate() {
        int[] numbersTrue = {10, 15, 20};
        long[] expectedTrue = {3628800, 1307674368000L, 2432902008176640000L};
    
        for (int i = 0; i < numbersTrue.length; i++) {
            Assert.assertEquals(expectedTrue[i], NumbersHelper.factorial(numbersTrue[i]));
        }
    
        int[] numbersFalse = {10, 15, 20};
        long[] expectedFalse = {10, 15, 22};
    
        for (int i = 0; i < numbersFalse.length; i++) {
            Assert.assertNotEquals(expectedFalse[i], NumbersHelper.factorial(numbersFalse[i]));
        }
    }
}
