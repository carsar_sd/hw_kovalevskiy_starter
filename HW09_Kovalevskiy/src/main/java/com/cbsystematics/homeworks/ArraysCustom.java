package com.cbsystematics.homeworks;

import java.util.Arrays;
import java.util.Collections;

/**
 * ArraysCustom Class presents homework on simple array manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ArraysCustom {
    
    public static void main(String[] args) {
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        String[] words = {
                ".", "порядке", "обратном", "в", "массива", "элементы", "все", "экран", "на", "выведите", "и",
                "элементов", "N", "в", "размерностью", "массив", "Создайте",
        };
        int[] items = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    
        System.out.println("1. " + Arrays.toString(ArraysCustom.first(numbers)));
        System.out.println("2: " + Arrays.toString(ArraysCustom.second(words))
                .replace(",", "")
                .replace(" .", ".")
        );
        System.out.println("3: " + Arrays.toString(ArraysCustom.third(items)));
    }
    
    /**
     * The method takes an array of integers and prints reversed array using Collections
     *
     * @param numbers
     *
     * @return reversed array
     */
    public static Integer[] first(Integer[] numbers)
    {
        Collections.reverse(Arrays.asList(numbers));
        
        return numbers;
    }
    
    /**
     * The method takes an array of integers and print reversed array using Loop
     *
     * @param words
     *
     * @return reversed array
     */
    public static String[] second(String[] words)
    {
        int length = words.length;
        String[] newArray = new String[length];
    
        for (int i = length - 1; i >= 0; i--) {
            newArray[length - i - 1] = words[i];
        }
        
        return newArray;
    }
    
    /**
     * The method takes an array of integers and print reversed array using Loop
     *
     * @param items
     *
     * @return reversed array
     */
    public static int[] third(int[] items)
    {
        int i = 0, j = items.length - 1;
    
        while (i < j) {
            int tmp = items[i];
            items[i] = items[j];
            items[j] = tmp;
            i++; j--;
        }
    
        return items;
    }
}
