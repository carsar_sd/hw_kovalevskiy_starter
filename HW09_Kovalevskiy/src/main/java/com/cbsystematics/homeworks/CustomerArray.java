package com.cbsystematics.homeworks;

import com.cbsystematics.homeworks.helpers.ScannerHelper;

import java.util.Arrays;
import java.util.Scanner;

/**
 * CustomerArray Class presents homework on simple array manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class CustomerArray {
    
    public static void main(String[] args) {
        int[] numbers = CustomerArray.getArrayFromConsole(7);
        
        System.out.println(Arrays.toString(numbers));
        System.out.println("Max: " + CustomerArray.getMax(numbers));
        System.out.println("Min: " + CustomerArray.getMin(numbers));
        System.out.println("Avg: " + CustomerArray.getAvg(numbers));
        System.out.println("Odd: " + CustomerArray.getOdd(numbers));
    }
    
    /**
     * The method takes an array of integers and returns max element
     *
     * @param numbers array of integers
     * @return max element
     */
    public static int getMax(int[] numbers) {
        return Arrays.stream(numbers).max().orElse(0);
    }
    
    /**
     * The method takes an array of integers and returns min element
     *
     * @param numbers array of integers
     * @return min element
     */
    public static int getMin(int[] numbers) {
        return Arrays.stream(numbers).min().orElse(0);
    }
    
    /**
     * The method takes an array of integers and returns average value of elements
     *
     * @param numbers array of integers
     * @return average value of elements
     */
    public static double getAvg(int[] numbers) {
        return Arrays.stream(numbers).average().orElse(0);
    }
    
    /**
     * The method takes an array of integers and returns odd elements
     *
     * @param numbers array of integers
     * @return odd elements
     */
    public static String getOdd(int[] numbers) {
        // stream filter Predicate is FunctionalInterface
        return Arrays.toString(Arrays.stream(numbers).filter(i -> i % 2 != 0).toArray());
    }
    
    /**
     * The method takes length and returns an array of given length
     *
     * @param length of the required array
     * @return an array of given length
     */
    public static int[] getArrayFromConsole(int length) {
        int[] numbers = new int[length];
        
        System.out.println("" +
                "-> Hello! This is a simple program that works with an array.\n" +
                "-> You can use an integer to create an array.\n" +
                "-> If you want to exit this program - type \"exit\" !\n"
        );
        
        Scanner in = new Scanner(System.in);
        
        for (int i = 0; i < length; i++) {
            numbers[i] = CustomerArray.getConsoleIntData(in);
        }
        
        in.close();
        
        return numbers;
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @return integer console data
     */
    public static int getConsoleIntData(Scanner in) {
        boolean isCorrectNumber;
        int number = 0;
        
        do {
            System.out.print("-> Enter a number: ");
            isCorrectNumber = in.hasNextInt();
            
            if (isCorrectNumber) {
                number = in.nextInt();
                System.out.println("-> OK!");
            } else {
                if (in.next().equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
                
                System.out.println("-> NOT OK! Your number is incorrect!");
                in.nextLine();
            }
        } while (!isCorrectNumber);
        
        return number;
    }
}
