package com.cbsystematics.homeworks;

import java.util.Arrays;
import java.util.Collections;

/**
 * ReversedArray Class presents homework on simple array manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ReversedArray {
    
    public static void main(String[] args) {
        Integer[] arr1 = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] arr2 = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        
        System.out.println(
                Arrays.toString(arr1) + " -> " + Arrays.toString(ReversedArray.myReverse(arr1))
        );
        System.out.println(
                Arrays.toString(arr2) + " -> " + Arrays.toString(ReversedArray.subArray(arr2, 3, 10))
        );
    }
    
    /**
     * The method takes an array of integers and returns reversed array using Collections
     *
     * @return new array
     */
    public static Integer[] myReverse(Integer[] array) {
        Collections.reverse(Arrays.asList(array));
        return array;
    }
    
    /**
     * The method takes an array of integers and returns new array of the passed arguments
     *
     * @param array
     * @param index
     * @param count
     *
     * @return new array
     */
    public static int[] subArray(int[] array, int index, int count) {
        int length = Math.max(array.length - index, count);
        int[] newArray = new int[length];
        
        Arrays.fill(newArray, 1);
        System.arraycopy(array, index, newArray, 0, array.length - index);
        
        return newArray;
    }
}
