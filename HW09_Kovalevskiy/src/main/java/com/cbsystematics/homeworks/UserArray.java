package com.cbsystematics.homeworks;

import java.util.Arrays;

/**
 * UserArray Class presents homework on simple array manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class UserArray {
    
    public static void main(String[] args) {
        int[] numbers = {1, 23, 12, 4, 6};
        int first = 13;
        
        System.out.println("-> old array: " + Arrays.toString(numbers) + " + " + first);
        System.out.println("-> new array: " + Arrays.toString(UserArray.getNewArray(numbers, first)));
    }
    
    /**
     * The method takes an array of integers and returns new array of the passed arguments
     *
     * @param numbers array of integers
     * @param first element of the new array
     * @return new array
     */
    public static int[] getNewArray(int[] numbers, int first) {
        int[] newArray = new int[numbers.length + 1];
        System.arraycopy(numbers, 0, newArray, 1, numbers.length);
        newArray[0] = first;
        
        return newArray;
    }
}
