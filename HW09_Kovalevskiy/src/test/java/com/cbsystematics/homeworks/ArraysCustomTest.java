package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class ArraysCustomTest {
    
    @Test
    public void testFirst() {
        Integer[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Integer[] expectedTrue = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        Assert.assertArrayEquals(expectedTrue, ArraysCustom.first(numbersTrue));
    
        Integer[] numbersFalse = {10, 15, 20};
        Integer[] expectedFalse = {10, 15, 22};
        Assert.assertNotEquals(expectedFalse, ArraysCustom.first(numbersFalse));
    }
    
    @Test
    public void testSecond() {
        String[] stringsTrue = {"!", "право", "на", "слева", "написано", "все"};
        String[] expectedTrue = {"все", "написано", "слева", "на", "право", "!"};
        Assert.assertArrayEquals(expectedTrue, ArraysCustom.second(stringsTrue));
    
        String[] stringsFalse = {"СТАРТ", "три", "два", "один"};
        String[] expectedFalse = {"три", "два", "один"};
        Assert.assertNotEquals(expectedFalse, ArraysCustom.second(stringsFalse));
    }
    
    @Test
    public void testThird() {
        int[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] expectedTrue = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        Assert.assertArrayEquals(expectedTrue, ArraysCustom.third(numbersTrue));
        
        int[] numbersFalse = {10, 15, 20};
        int[] expectedFalse = {10, 15, 22};
        Assert.assertNotEquals(expectedFalse, ArraysCustom.third(numbersFalse));
    }
}
