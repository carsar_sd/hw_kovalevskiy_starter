package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class CustomerArrayTest {
    
    @Test
    public void testGetMax() {
        int[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int expectedTrue = 9;
        Assert.assertEquals(expectedTrue, CustomerArray.getMax(numbersTrue));
    
        int[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int expectedFalse = 5;
        Assert.assertNotEquals(expectedFalse, CustomerArray.getMax(numbersFalse));
    }
    
    @Test
    public void testGetMin() {
        int[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int expectedTrue = 0;
        Assert.assertEquals(expectedTrue, CustomerArray.getMin(numbersTrue));
        
        int[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int expectedFalse = 5;
        Assert.assertNotEquals(expectedFalse, CustomerArray.getMin(numbersFalse));
    }
    
    @Test
    public void testGetAvg() {
        int[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        double expectedTrue = 4.5;
        Assert.assertEquals(expectedTrue, CustomerArray.getAvg(numbersTrue), 0);
        
        int[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        double expectedFalse = 22;
        Assert.assertNotEquals(expectedFalse, CustomerArray.getAvg(numbersFalse), 0);
    }
    
    @Test
    public void testGetOdd() {
        int[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        String expectedTrue = "[1, 3, 5, 7, 9]";
        Assert.assertEquals(expectedTrue, CustomerArray.getOdd(numbersTrue));
        
        int[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        String expectedFalse = "test";
        Assert.assertNotEquals(expectedFalse, CustomerArray.getOdd(numbersFalse));
    }
}
