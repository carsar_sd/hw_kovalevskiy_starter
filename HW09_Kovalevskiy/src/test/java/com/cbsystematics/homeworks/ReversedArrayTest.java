package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class ReversedArrayTest {
    
    @Test
    public void testMyReverse() {
        Integer[] numbersTrue = {1, 23, 12, 4, 6};
        Integer[] expectedTrue = {6, 4, 12, 23, 1};
        Assert.assertArrayEquals(expectedTrue, ReversedArray.myReverse(numbersTrue));
    
        Integer[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Integer[] expectedFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Assert.assertNotEquals(expectedFalse, ReversedArray.myReverse(numbersFalse));
    }
    
    @Test
    public void testSubArray() {
        int[] numbersTrue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] expectedTrue = {3, 4, 5, 6, 7, 8, 9, 1, 1, 1};
        Assert.assertArrayEquals(expectedTrue, ReversedArray.subArray(numbersTrue, 3, 10));
    
        int[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] expectedFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Assert.assertNotEquals(expectedFalse, ReversedArray.subArray(numbersFalse, 5, 10));
    }
}
