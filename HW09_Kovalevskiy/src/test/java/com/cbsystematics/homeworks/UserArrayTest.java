package com.cbsystematics.homeworks;

import org.junit.Assert;
import org.junit.Test;

public class UserArrayTest {
    
    @Test
    public void testGetNewArray() {
        int[] numbersTrue = {1, 23, 12, 4, 6};
        int[] expectedTrue = {18, 1, 23, 12, 4, 6};
        Assert.assertArrayEquals(expectedTrue, UserArray.getNewArray(numbersTrue, 18));
        
        int[] numbersFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] expectedFalse = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Assert.assertNotEquals(expectedFalse, UserArray.getNewArray(numbersFalse, 10));
    }
}
